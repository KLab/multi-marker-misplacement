# -*- coding: utf-8 -*-
"""
Authors: M. Bergere, M Fonseca
        Kinesiology Laboratory (K-LAB)
        University of Geneva
        https://www.unige.ch/medecine/kinesiology

License:   Creative Commons Attribution-NonCommercial 4.0 International License 
           https://creativecommons.org/licenses/by-nc/4.0/legalcode

Source code:   https://gitlab.unige.ch/KLab/ (...)

Reference  :   "Impact of knee lower limb misplacement on gait kinematics of children with cerebral palsy using the Conventional Gait Model - A sensitivity study" 
               M. Fonseca, X. Gasparutto, F. Leboeuf, R. Dumas, S. Armand; Plos One
Date       :   September 2022

Dependencies: 
        https://github.com/pyCGM2/pyCGM2.git
        git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

Description: 
        This routine calculates the kinematics of a dynamic file, adds a n number of marker misplacement and posteriorly calculates the kinematics of the new simulated markers
        misplacment. Then stores all the calculated kinematics as csv files. This code is adapted for use in the Baobab server of the University of Geneva, and thus adapted to 
        be run in Ubuntu.
Process:
        1- Preparation of files, variables and paths
        2- Import .C3D files
            2.1- Simplify static and dynamic files (delete useless marker coordinates, metadata, reduce frame rate, etc)
            2.2- Compute kinematics for the original postion, using pyCGM2
            2.3- Get initial angles 
            2.4- Store inital angles in CSV
        3- Compute simulations (loop on the defined simulations imported from the command_file.txt)
            2.1- Create new .C3D files (copies) of
            2.2- Add misplacement to the markers according to command_file.txt
            2.3- Compute kinematics, using pyCGM2
            2.4- Get the new computed angles
            2.5- Store the simulated angles as .csv
"""
from __future__ import print_function
from __future__ import division
from __future__ import absolute_import 

import os
import glob
import Definition
import Simplify_acq
import File_Processing
import Misplacement_M
import Point_Manipulation
import pyCGM2_CGM11_modelling
import UserSettings
import time
import Write_Result_File
import numpy as np
import Analysis
import argparse
import time


def TicTocGenerator():
    # Generator that returns time differences
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference

TicToc = TicTocGenerator() # create an instance of the TicTocGen generator

# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        print( "Elapsed time: %f seconds.\n" %tempTimeInterval )

def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)

# define argparse to execute pyCGM modelling
parser = argparse.ArgumentParser(description='CGM1.1-pipeline')
parser.add_argument('-uf', '--userFile', type=str, help='userSettings', default="CGM1_1.userSettings")
parser.add_argument('-ef', '--expertFile', type=str, help='Local expert settings', default=None)
parser.add_argument('--start_combi', type=int, help='Start combi index', default=1)
parser.add_argument('--end_combi', type=int, help='end combi index', default=390027)
args = parser.parse_args()

args.end_combi = min(args.end_combi, 390027)

#1- PREPARATION OF FILES, VARIABLES, PATH
    
current_path=os.path.join(os.getcwd(), f"data_tmp_{args.start_combi}")
data_path=os.path.join(os.getcwd(), f"data_tmp_{args.start_combi}")
result_path='Results'
# os.makedirs(result_path, exist_ok=True)
if not os.path.exists(result_path):
    os.makedirs(result_path)

    #Get markers to misplace and combinaison of errors
method=Definition.definition_method(current_path)
if method.rsplit('/',1)[-1]=='command_file': # take only name of method 
    Combi=Definition.definition_combi(current_path)[0]
else: 
    #method=='command_file_Della_Croce'
    Combi=Definition.definition_combi_Della_Croce(current_path)[0]
MARK=Definition.definition_marker(current_path, method.rsplit('/',1)[-1])[3]

    #Get static and dynamic files
Static_files=glob.glob(data_path+'/'+'*'+'SB'+'*.C3D')
Static_files = [sf for sf in Static_files if 'MIS' not in sf]
Dyn_files=glob.glob(data_path+'/'+'*'+'GB'+'*.C3D')
Dyn_files = [df for df in Dyn_files if 'MIS' not in df]

Angle_names=['Pelvis tilt','Pelvis obliquity','Pelvis rotation',\
                   'Hip flexion/extention','Hip adduction/abduction','Hip rotation',\
                   'Knee flexion/extention','Knee valgus/varus','Knee rotation',\
                   'Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']
PelvisAngles_x=[]
PelvisAngles_y=[]
PelvisAngles_z=[]
HipAngles_x=[]
HipAngles_y=[]
HipAngles_z=[]
KneeAngles_x=[]
KneeAngles_y=[]
KneeAngles_z=[]
AnkleAngles_x=[]
AnkleAngles_z=[]
Foot_Pro=[]
Angles=[PelvisAngles_x,PelvisAngles_y,PelvisAngles_z,\
                 HipAngles_x,HipAngles_y,HipAngles_z,\
                 KneeAngles_x,KneeAngles_y,KneeAngles_z,\
                 AnkleAngles_x,AnkleAngles_z,Foot_Pro]
                 
    #Creation of .txt file with simulation number and combinaison of marker positions associated
C=[]
suffix = args.start_combi
first_suffix = suffix
Combi=Combi[args.start_combi:args.end_combi]
for c in Combi:  
    C.append([suffix, '->', c ])
    suffix+=1
Write_Result_File.combinaisons(result_path, C)
T_combi=[]

            #1. GET SIMPLE C3D FILES AND REFERENCE
for p in range (1,len(Static_files)+1):
    nfile = Static_files[p-1][-41:-39]
    patient_result_path=result_path+"/Patient"+nfile
    if not os.path.exists(patient_result_path):
        os.mkdir(patient_result_path)
    analysis_path=patient_result_path+"/Analysis files"
    if not os.path.exists(analysis_path):
        os.mkdir(analysis_path)
    print (nfile)
    stat=Static_files[p-1]
    # dyn=Dyn_files[p-1] 
    subs=stat[-41:-23]
    dyn=[i for i in Dyn_files if subs in i]
    dyn=dyn[0]
    print (stat)
    print (dyn)
    
    s=stat.replace(data_path+'/', '')
    s=s.replace('.C3D', '')
    d=dyn.replace(data_path+'/', '')
    d=d.replace('.C3D', '')
    
        #1.1 Simplifie static and dynamic files 
    Point_To_Keep=['LTOE','LHEE','LMET','LMED','LMIF','LANK','LTIB',\
                   'RTOE','RHEE','RMET','RMED','RMIF','RANK','RTIB','RTTU','RKNE','RTHI',\
                   'LTTU','LKNE','LTHI','LASI','RASI','LPSI','RPSI']
    Simplify_acq.new_dyn(dyn,Point_To_Keep) 
#    Simplify_acq.new_stat(stat,Point_To_Keep,100) 
    Simplify_acq.clear_acq(dyn)
    Simplify_acq.clear_acq(stat)
    
        #1.2 pyCGM computation for original positions
    UserSettings.user_settings(dyn, current_path, s, d , f'pyCGM1', additional_suffix=f"-{args.start_combi}-{args.end_combi}")
    args.userFile = f"CGM1_1.userSettings_{d[:2]}_pyCGM1-{args.start_combi}-{args.end_combi}"
    pyCGM2_CGM11_modelling.main(args)
    
        #1.3 Get initial pyCGM angles (reference) 
    acq_dyn=File_Processing.reader(dyn)
    
    LPelvisAngles=acq_dyn.GetPoint("LPelvisAngles_pyCGM1").GetValues() 
    LHipAngles=acq_dyn.GetPoint("LHipAngles_pyCGM1").GetValues() 
    LKneeAngles=acq_dyn.GetPoint("LKneeAngles_pyCGM1").GetValues() 
    LAnkleAngles=[]
    for frame in range(len(LKneeAngles[:,0])):
        LAnkleAngles.append([acq_dyn.GetPoint("LAnkleAngles_pyCGM1").GetValues()[frame,0],acq_dyn.GetPoint("LAnkleAngles_pyCGM1").GetValues()[frame,2]])
    LAnkleAngles=np.array(LAnkleAngles)
    LFootPro=[]
    for frame in range(len(LKneeAngles[:,0])):
        LFootPro.append([acq_dyn.GetPoint("LFootProgressAngles_pyCGM1").GetValues()[frame,2]])
    LFootPro=np.array(LFootPro)
    
    Angle_ori=[LPelvisAngles,LHipAngles,LKneeAngles,LAnkleAngles,LFootPro]
    Angle_Values_ori=[]
    for angle in Angle_ori:
        angle=angle.T
        for dir in angle:
            Angle_Values_ori.append(dir)
    for a in range(len(Angles)):
        Angles[a].append(Angle_Values_ori[a])
        
        #1.4 Store misplaced angles in csv files
    Write_Result_File.store_angles(Angle_names, Angle_Values_ori, "_pyCGM1", nfile, patient_result_path)
    Point_Manipulation.remove_angles(acq_dyn, "pyCGM1") 
    File_Processing.writer(acq_dyn,dyn)  
    
    
            
        #2- COMPUTATION OF SIMULATION
    first_suffix=args.start_combi
    for c in Combi:
        # tic()
            #2.1 Creation of new files, new file names
        new_stat=stat.replace('.C3D', f'_MIS_{first_suffix}')+'.C3D'
        new_dyn=dyn.replace('.C3D', f'_MIS_{first_suffix}')+'.C3D'
    
        new_s=new_stat.replace(data_path+'/', '')
        new_s=new_s.replace('.C3D', '')
        new_d=new_dyn.replace(data_path+'/', '')
        new_d=new_d.replace('.C3D', '')

        tcombi0=time.time() 
        acq_stat=File_Processing.reader(stat)   
        acq_dyn=File_Processing.reader(dyn) 
        
            #2.1 Misplace markers
        for m in range(len(MARK)):  
            a=c[m][1][0]
            e=c[m][1][1]
            mark=c[m][0]
            Misplacement_M.move(acq_stat, mark, m, a, e, current_path, method)
            Misplacement_M.move(acq_dyn, mark, m, a, e, current_path, method)
            File_Processing.writer(acq_stat,new_stat)
            File_Processing.writer(acq_dyn,new_dyn)
                  
           #2.2 Remove virtual markers to compute them again from misplaced marker set
        acq_dyn=File_Processing.reader(new_dyn)        
        Point_Manipulation.remove_virtual_makers(acq_dyn)
        File_Processing.writer(acq_dyn,new_dyn)
        
        acq_stat=File_Processing.reader(new_stat)
        Point_Manipulation.remove_virtual_makers(acq_stat)       
        File_Processing.writer(acq_stat,new_stat) 

            #2.3 Kinematics computation
        suffix_us = first_suffix-1
        if suffix_us == args.start_combi-1:
            suffix_us = f'pyCGM1-{args.start_combi}-{args.end_combi}'
        suffix = first_suffix
        UserSettings.set_user_settings(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix_us}", new_s, new_d , str(suffix))
        tic()
        args.userFile = f"CGM1_1.userSettings_{new_d[:2]}_{suffix}"
        pyCGM2_CGM11_modelling.main(args)
        toc()
            #2.4 Get new angles 
        Angle_Values_mis=[]
        acq_dyn=File_Processing.reader(new_dyn)
         
        LPelvisAngles_mis=acq_dyn.GetPoint("LPelvisAngles_"+str(suffix)).GetValues() 
        LHipAngles_mis=acq_dyn.GetPoint("LHipAngles_"+str(suffix)).GetValues() 
        LKneeAngles_mis=acq_dyn.GetPoint("LKneeAngles_"+str(suffix)).GetValues() 
        LAnkleAngles_mis=acq_dyn.GetPoint("LAnkleAngles_"+str(suffix)).GetValues()[0]+acq_dyn.GetPoint("LAnkleAngles_"+str(suffix)).GetValues()[2]
        LFootPro_mis=acq_dyn.GetPoint("LFootProgressAngles_"+str(suffix)).GetValues()[3]
        
        LAnkleAngles_mis=[]
        for frame in range(len(LKneeAngles_mis[:,0])):
            LAnkleAngles_mis.append([acq_dyn.GetPoint("LAnkleAngles_"+str(suffix)).GetValues()[frame,0],acq_dyn.GetPoint("LAnkleAngles_"+str(suffix)).GetValues()[frame,2]])
        LAnkleAngles_mis=np.array(LAnkleAngles_mis)
        LFootPro_mis=[]
        for frame in range(len(LKneeAngles_mis[:,0])):
            LFootPro_mis.append([acq_dyn.GetPoint("LFootProgressAngles_"+str(suffix)).GetValues()[frame,2]])
        LFootPro_mis=np.array(LFootPro_mis)
        
        Angle_mis=[LPelvisAngles_mis,LHipAngles_mis,LKneeAngles_mis,LAnkleAngles_mis,LFootPro_mis] 
        for angle in Angle_mis:
            angle=angle.T
            for dir in angle:
                Angle_Values_mis.append(dir)
        for a in range(len(Angles)):
            Angles[a].append(Angle_Values_mis[a])

        if os.path.exists(new_dyn):
            os.remove(new_dyn)
        if os.path.exists(new_stat):
            os.remove(new_stat)
        if os.path.exists(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix_us}"):
            os.remove(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix_us}")
            #2.5 Store new angles in csv files (15000 files per folder)
    #    if first_suffix>sEnd:
    #        sInit=first_suffix
    #        sEnd+=len_folder
    #        separate_folder=patient_result_path+'\\results '+str(sInit)+' to '+str(sEnd)
    #        os.mkdir(separate_folder)
        Write_Result_File.store_angles(Angle_names, Angle_Values_mis, "simu"+str(suffix), nfile, patient_result_path)
        
        first_suffix+=1
        tcombi1=time.time()
        T_combi.append(tcombi1-tcombi0)
    try:
        new_d
    except:
        print("Variable new_d is not defined")
    else:
        if os.path.exists(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix}"):
            os.remove(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix}")

    # if os.path.exists(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix}"):
    #     os.remove(current_path+f"/CGM1_1.userSettings_{new_d[:2]}_{suffix}")
    # Analysis.write_analysis_files(p, patient_result_path, patient_result_path+'/Analysis files', Angle_names)
    
