# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 12:38:19 2020

@author: MCDF
"""
import btk

def reader(file):
    reader = btk.btkAcquisitionFileReader()
    reader.SetFilename(file) 
    reader.Update()
    acq=reader.GetOutput()
    return acq

def store_angles(Angle_Names, Values, simu, patient, result_path):
    if patient<=9:
        pat=str(0)+str(patient)
    else:
        pat=str(patient)
    f=open(result_path+"\\"+"p"+pat+"_"+ str(simu)+'.csv', 'w')  
    frame_number=len(Values[0])
    for angle in Values:
        if len(angle)<frame_number:
            frame_number=len(angle)
    f.write(";") 
    for angle_name in Angle_Names:
        f.write(angle_name+";")
    f.write("\n")
    
    for frame in range(frame_number):
        f.write("frame "+str(frame+1)+";")
        for angle in range(len(Values)):
            f.write(str(Values[angle][frame])+";")
        f.write("\n")
    f.close()
