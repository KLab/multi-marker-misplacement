# -*- coding: utf-8 -*-
"""
@ M.Fonseca Feb 2020

Description: Routine adapted for writting multiple dynamic files

#    file: path and c3d file name (static or dyn; to get metadata)
#    settings_path: path to write user setting (current path)
#    static_file: path and c3d static file name
#    dynamic_files: path and c3d dynamic file name
#    suffix: used as int, can be str

"""



import yaml
import File_Processing

def user_settings(settings_path, static_file, dynamic_files, suffix, settings_name):
    '''Creates CGM1_1.userSettings to run pyCGM'''
    acq=File_Processing.reader(dynamic_files[0])
    
    Required_to_write=["Bodymass","Height","LeftLegLength","RightLegLength","LeftKneeWidth","RightKneeWidth","LeftAnkleWidth","RightAnkleWidth","LeftSoleDelta","RightSoleDelta","LeftShoulderOffset","LeftElbowWidth","LeftWristWidth","LeftHandThickness","RightShoulderOffset","RightElbowWidth","RightWristWidth","RightHandThickness"]
    Optional_to_write=["InterAsisDistance","LeftAsisTrocanterDistance","LeftTibialTorsion","LeftThighRotation","LeftShankRotation","RightAsisTrocanterDistance","RightTibialTorsion","RightThighRotation","RightShankRotation","LeftKneeFuncCalibrationOffset","RightKneeFuncCalibrationOffset"]
    
    dictionary={}
    
    with open(str(settings_path)+"\\"+settings_name,"w") as f:
        
        MP={}
        Required={}
        Optional={}
        Global={}
        metadata=acq.GetMetaData()
        for r in Required_to_write:
            Required[str(r)]=0
        for o in Optional_to_write:
               Optional[str(o)]=0
        Required["Bodymass"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[0]
        Required["Height"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[1]
        Required["LeftLegLength"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[4]
        Required["RightLegLength"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[7]
        Required["LeftKneeWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[3]
        Required["RightKneeWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[6]
        Required["LeftAnkleWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[2]
        Required["RightAnkleWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[5]
        MP[str("Required")]=Required
        MP[str("Optional")]=Optional
        dictionary["MP"]=MP
        Global["Marker diameter"]=14
        Global["Point suffix"]= suffix
        dictionary["Global"]=Global
        Calibration={}
        Calibration["StaticTrial"]=static_file.replace(settings_path,'')#+'.C3D'
        Calibration["Left flat foot"]=True
        Calibration["Right flat foot"]=True
        Calibration["Head flat"]=True
        dictionary["Calibration"]=Calibration
        Fitting={}
        Trials=[]
        File={}
        for i in range(len(dynamic_files)): 
            File["File"]=dynamic_files[i]#+'.C3D'
            File["Mfpa"]="XX"
            Trials.append(File.copy())
        
        Fitting["Trials"]=Trials
        dictionary["Fitting"]=Fitting
        f.write(yaml.dump(dictionary, indent=4))
    f.close()

def user_settings2(settings_path, static_file, dynamic_files, suffix, settings_name):
    '''
        Function:
            -Creates CGM1_1.userSettings to run pyCGM
            
        Variables:
            settings_path:
            static_file:
            dynamic_files:
            suffix:
            settings_name:
            
        Description:
        
        '''
    acq=File_Processing.reader(static_file)
    
    Required_to_write=["Bodymass","Height","LeftLegLength","RightLegLength","LeftKneeWidth","RightKneeWidth","LeftAnkleWidth","RightAnkleWidth","LeftSoleDelta","RightSoleDelta","LeftShoulderOffset","LeftElbowWidth","LeftWristWidth","LeftHandThickness","RightShoulderOffset","RightElbowWidth","RightWristWidth","RightHandThickness"]
    Optional_to_write=["InterAsisDistance","LeftAsisTrocanterDistance","LeftTibialTorsion","LeftThighRotation","LeftShankRotation","RightAsisTrocanterDistance","RightTibialTorsion","RightThighRotation","RightShankRotation","LeftKneeFuncCalibrationOffset","RightKneeFuncCalibrationOffset"]
    
    dictionary={}
    
    with open(str(settings_path)+"\\"+settings_name,"w") as f:
        
        MP={}
        Required={}
        Optional={}
        Global={}
        metadata=acq.GetMetaData()
        for r in Required_to_write:
            Required[str(r)]=0
        for o in Optional_to_write:
            Optional[str(o)]=0
        Required["Bodymass"]=metadata.FindChild("SUBJECTS").value().FindChild("A_BodyMass_kg").value().GetInfo().ToDouble()[0]
        Required["Height"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Height_mm").value().GetInfo().ToDouble()[0]
        Required["LeftLegLength"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Left_LegLength_mm").value().GetInfo().ToDouble()[0]
        Required["RightLegLength"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Right_LegLength_mm").value().GetInfo().ToDouble()[0]
        Required["LeftKneeWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Left_KneeWidth_mm").value().GetInfo().ToDouble()[0]
        Required["RightKneeWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Right_KneeWidth_mm").value().GetInfo().ToDouble()[0]
        Required["LeftAnkleWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Left_AnkleWidth_mm").value().GetInfo().ToDouble()[0]
        Required["RightAnkleWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Right_AnkleWidth_mm").value().GetInfo().ToDouble()[0]
        Optional["InterAsisDistance"]=metadata.FindChild("SUBJECTS").value().FindChild("A_InterAsisDistance_mm").value().GetInfo().ToDouble()[0]

        MP[str("Required")]=Required
        MP[str("Optional")]=Optional
        dictionary["MP"]=MP
        # if hasattr(metadata.FindChild("SUBJECTS").value(), "MARKER_DIAMETER"):
        Global["Marker diameter"]= float(metadata.FindChild("SUBJECTS").value().FindChild("MARKER_DIAMETER").value().GetInfo().ToString()[0][0:-2])
        # else:
        # Global["Marker diameter"]=16

        Global["Point suffix"]= suffix
        dictionary["Global"]=Global
        Calibration={}
        Calibration["StaticTrial"]=static_file.replace(settings_path,'')#+'.C3D'
        
        Calibration["Left flat foot"]=metadata.FindChild("SUBJECTS").value().FindChild("LEFT_FLATFOOT").value().GetInfo().ToString()[0]
        Calibration["Right flat foot"]=metadata.FindChild("SUBJECTS").value().FindChild("RIGHT_FLATFOOT").value().GetInfo().ToString()[0]
        # Calibration["Left flat foot"]=True
        # Calibration["Right flat foot"]=True
        Calibration["Head flat"]=True
        dictionary["Calibration"]=Calibration
        Fitting={}
        Trials=[]
        File={}
        for i in range(len(dynamic_files)): 
            File["File"]=dynamic_files[i]#+'.C3D'
            File["Mfpa"]="XX"
            Trials.append(File.copy())
        
        Fitting["Trials"]=Trials
        dictionary["Fitting"]=Fitting
        f.write(yaml.dump(dictionary, indent=4))
            
#        elif metadata.FindChild("MANUFACTURER").value().FindChild("COMPANY").value().GetInfo().ToString()[0] == 'Vicon':
#            Required["Bodymass"] = metadata.FindChild("PROCESSING").value().FindChild("Bodymass").value().GetInfo().ToDouble()[0]
#            Required["Height"]   = metadata.FindChild("PROCESSING").value().FindChild("Height").value().GetInfo().ToDouble()[0]
#            Required["LeftLegLength"] = metadata.FindChild("PROCESSING").value().FindChild("LLegLength").value().GetInfo().ToDouble()[0]
#            Required["RightLegLength"]=metadata.FindChild("PROCESSING").value().FindChild("RLegLength").value().GetInfo().ToDouble()[0]
#            Required["LeftKneeWidth"]=metadata.FindChild("PROCESSING").value().FindChild("LKneeWidth").value().GetInfo().ToDouble()[0]
#            Required["RightKneeWidth"]=metadata.FindChild("PROCESSING").value().FindChild("RKneeWidth").value().GetInfo().ToDouble()[0]
#            Required["LeftAnkleWidth"]=metadata.FindChild("PROCESSING").value().FindChild("LAnkleWidth").value().GetInfo().ToDouble()[0]
#            Required["RightAnkleWidth"]=metadata.FindChild("PROCESSING").value().FindChild("RAnkleWidth").value().GetInfo().ToDouble()[0]
#            MP[str("Required")]=Required
#            MP[str("Optional")]=Optional
#            dictionary["MP"]=MP
#            Global["Marker diameter"]=14
#            Global["Point suffix"]= suffix
#            dictionary["Global"]=Global
#            Calibration={}
#            Calibration["StaticTrial"]=static_file.replace(settings_path,'')#+'.C3D'
#            Calibration["Left flat foot"]=True
#            Calibration["Right flat foot"]=True
#            dictionary["Calibration"]=Calibration
#            Fitting={}
#            Trials=[]
#            File={}
#            for i in range(len(dynamic_files)): 
#                File["File"]=dynamic_files[i]#+'.C3D'
#                File["Mfpa"]="XX"
#                Trials.append(File.copy())
#            
#            Fitting["Trials"]=Trials
#            dictionary["Fitting"]=Fitting
#            f.write(yaml.dump(dictionary, indent=4))          
    f.close()
def user_settings2_2(settings_path, static_file, dynamic_file, suffix, settings_name):
    '''Creates CGM1_1.userSettings to run pyCGM read 1 dynamic file'''
    acq=File_Processing.reader(static_file)
    
    Required_to_write=["Bodymass","Height","LeftLegLength","RightLegLength","LeftKneeWidth","RightKneeWidth","LeftAnkleWidth","RightAnkleWidth","LeftSoleDelta","RightSoleDelta","LeftShoulderOffset","LeftElbowWidth","LeftWristWidth","LeftHandThickness","RightShoulderOffset","RightElbowWidth","RightWristWidth","RightHandThickness"]
    Optional_to_write=["InterAsisDistance","LeftAsisTrocanterDistance","LeftTibialTorsion","LeftThighRotation","LeftShankRotation","RightAsisTrocanterDistance","RightTibialTorsion","RightThighRotation","RightShankRotation","LeftKneeFuncCalibrationOffset","RightKneeFuncCalibrationOffset"]
    
    dictionary={}
    
    with open(str(settings_path)+"\\"+settings_name,"w") as f:
        
        MP={}
        Required={}
        Optional={}
        Global={}
        metadata=acq.GetMetaData()
        for r in Required_to_write:
            Required[str(r)]=0
        for o in Optional_to_write:
            Optional[str(o)]=0
        Required["Bodymass"]=metadata.FindChild("SUBJECTS").value().FindChild("A_BodyMass_kg").value().GetInfo().ToDouble()[0]
        Required["Height"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Height_mm").value().GetInfo().ToDouble()[0]
        Required["LeftLegLength"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Left_LegLength_mm").value().GetInfo().ToDouble()[0]
        Required["RightLegLength"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Right_LegLength_mm").value().GetInfo().ToDouble()[0]
        Required["LeftKneeWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Left_KneeWidth_mm").value().GetInfo().ToDouble()[0]
        Required["RightKneeWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Right_KneeWidth_mm").value().GetInfo().ToDouble()[0]
        Required["LeftAnkleWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Left_AnkleWidth_mm").value().GetInfo().ToDouble()[0]
        Required["RightAnkleWidth"]=metadata.FindChild("SUBJECTS").value().FindChild("A_Right_AnkleWidth_mm").value().GetInfo().ToDouble()[0]
        Optional["InterAsisDistance"]=metadata.FindChild("SUBJECTS").value().FindChild("A_InterAsisDistance_mm").value().GetInfo().ToDouble()[0]
        MP[str("Required")]=Required
        MP[str("Optional")]=Optional
        dictionary["MP"]=MP
        Global["Marker diameter"]=14
        Global["Point suffix"]= suffix
        dictionary["Global"]=Global
        Calibration={}
        Calibration["StaticTrial"]=static_file.replace(settings_path,'')#+'.C3D'
        Calibration["Left flat foot"]=True
        Calibration["Right flat foot"]=True
        Calibration["Head flat"]=True
        dictionary["Calibration"]=Calibration
        Fitting={}
        Trials=[]
        File={}
         
        File["File"]=dynamic_file#+'.C3D'
        File["Mfpa"]="XX"
        Trials.append(File.copy())
        
        Fitting["Trials"]=Trials
        dictionary["Fitting"]=Fitting
        f.write(yaml.dump(dictionary, indent=4))
        
    f.close()
def user_settings3(settings_path, static_file, dynamic_files, suffix, settings_name):
    '''Creates CGM1_1.userSettings to run pyCGM'''
    acq=File_Processing.reader(dynamic_files[0])
    
    Required_to_write=["Bodymass","Height","LeftLegLength","RightLegLength","LeftKneeWidth","RightKneeWidth","LeftAnkleWidth","RightAnkleWidth","LeftSoleDelta","RightSoleDelta","LeftShoulderOffset","LeftElbowWidth","LeftWristWidth","LeftHandThickness","RightShoulderOffset","RightElbowWidth","RightWristWidth","RightHandThickness"]
    Optional_to_write=["InterAsisDistance","LeftAsisTrocanterDistance","LeftTibialTorsion","LeftThighRotation","LeftShankRotation","RightAsisTrocanterDistance","RightTibialTorsion","RightThighRotation","RightShankRotation","LeftKneeFuncCalibrationOffset","RightKneeFuncCalibrationOffset"]
    
    dictionary={}
    
    with open(str(settings_path)+"\\"+settings_name,"w") as f:
        
        MP={}
        Required={}
        Optional={}
        Global={}
        metadata=acq.GetMetaData()
        for r in Required_to_write:
            Required[str(r)]=0
        for o in Optional_to_write:
               Optional[str(o)]=0
        Required["Bodymass"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[0]
        Required["Height"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[1]
        Required["LeftLegLength"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[4]
        Required["RightLegLength"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[7]
        Required["LeftKneeWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[3]
        Required["RightKneeWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[6]
        Required["LeftAnkleWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[2]
        Required["RightAnkleWidth"]=metadata.FindChild("ANALYSIS").value().FindChild("VALUES").value().GetInfo().ToDouble()[5]
        MP[str("Required")]=Required
        MP[str("Optional")]=Optional
        dictionary["MP"]=MP
        Global["Marker diameter"]=14
        Global["Point suffix"]= suffix
        dictionary["Global"]=Global
        Calibration={}
        Calibration["StaticTrial"]=static_file.replace(settings_path,'')#+'.C3D'
        Calibration["Left flat foot"]=True
        Calibration["Right flat foot"]=True
        Calibration["Head flat"]=True
        dictionary["Calibration"]=Calibration
        Fitting={}
        Trials=[]
        File={}
        for i in range(len(dynamic_files)): 
            File["File"]=dynamic_files[i]#+'.C3D'
            File["Mfpa"]="XX"
            Trials.append(File.copy())
        
        Fitting["Trials"]=Trials
        dictionary["Fitting"]=Fitting
        f.write(yaml.dump(dictionary, indent=4))
    f.close()    
    
def set_user_settings(user_setting_file, static_file, dynamic_files, suffix):
    '''Modify existing CGM1_1.userSettings to run pyCGM. 
    Careful : only sets calibration and trial filenames, and suffix name'''
    with open(user_setting_file) as f:
        list_doc=yaml.load(f)
    f.close()
    list_doc['Calibration']['StaticTrial']=static_file+'.C3D'
    list_doc['Global']['Point suffix']=suffix
    list_doc['Fitting']['Trials'][0]['File']=dynamic_files+'.C3D'
    with open(user_setting_file, "w") as f:
        f.write(yaml.dump(list_doc, indent=4))
    f.close()
        
        
        
        
        
        
        
        