<table width="100%">
    <tr>
        <td width="25%">
            <img src="https://www.unige.ch/medecine/kinesiology/application/files/5615/7313/2686/logo_UNIGE_300.png" alt="Kinesiology Laboratory - University of Geneva" width="100%"/>
        </td>
        <td width="75%">
            <h1>Welcome on the K-LAB GitLab repository</h1><br>
            <p>Thank you for your interest in our projects. You can find further information about our research activites at the University of Geneva on our website: <a href="https://www.unige.ch/medecine/kinesiology" target="_blank">https://www.unige.ch/medecine/kinesiology</a>.</p>
            <p>The projects available on this repository are all freely available and opensource, under the license <a href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank">Creative Commons Attribution-NonCommercial 4.0 (International License)</a>. <strong>Not for clinical use</strong>.</p>
        </td>
    </tr>
</table>
<h2 align="left">PROJECT DESCRIPTION</h2>

Simulate multiple marker displacements and calculate its combined effect in kinematic output

</h2>

## Important Notices

* `master` branch file paths (if exist) are **not** considered stable.

## Table of Contents
[**Installation**](#installation)

[**Features**](#features)

[**Dependencies**](#dependencies)

[**Developer**](#developer)

[**Examples**](#examples)

[**References**](#references)

[**License**](#license)

## Installation

You just need to download or clone the project to use it. Just ensure that you are using Matlab R2018b or newer (The Mathworks, USA).

## Features


* MIS2_MAIN.py
	* Run this routine to apply marker misplacement
* command_file.txt
	* Define the markers to be misplaced, as well as magnitude and direction


## Dependencies


* https://github.com/pyCGM2/pyCGM2.git


## Developer

The proposed routine has been developed by <a href="https://www.unige.ch/medecine/kinesiology/people/mickaelf/" target="_blank">Mickael Fonseca</a> and Mariette Bergere, K-Lab, University of Geneva.

## References

<b><a href="10.1038/s41598-022-18546-5" target="_blank">Impact of lower limb marker misplacement on gait kinematics of children with cerebral palsy using the Conventional Gait Model - A sensitivity study</a></b><br>
M. Fonseca, M. Bergere, J. Candido, F. Labouf,  R. Dumas, S. Armand<br>
<i>Scientific Reports, August 2022, 12(1):14207</i>

## License

<a href="https://creativecommons.org/licenses/by-nc/4.0/legalcode" target="_blank">LICENSE</a> © K-Lab, University of Geneva
> Not for clinical use
