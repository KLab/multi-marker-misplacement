
"""
author: M. Fonseca

Description: Routine to create a bar chart containing the percentage of RMS, per angle, within 4 different intervals.

"""

# %%
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import glob

# %%
# data path
datapath = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Results\\RMS\\TDC\\"
RMS_list = glob.glob(datapath+'*.csv')
print(RMS_list)
z = [0]*12
x2 = [0]*12
x5 = [0]*12
x10 = [0]*12
x_up10 = [0]*12
print(x2)

def count_in_range(Z, A, B, C, D, column):
    for value in column:
        value = value * 1
        if value == 0:
            Z = Z + 1 
        elif value > 0 and value <2:
            A = A + 1
        elif value > 2 and value <5:
            B = B + 1
        elif  value > 5 and value <10:
            C = C + 1
        elif value >10:
            D = D + 1

    return Z, A, B, C, D

# loop read csv file
for file in RMS_list:
    rms_df = pd.read_csv(file)
    del rms_df['Simu']
    for c in range(0,12):
        col = rms_df.columns[c]
        # print(col)
        column_rms = (rms_df[col])
        [z[c], x2[c], x5[c], x10[c], x_up10[c]] = count_in_range(z[c], x2[c], x5[c], x10[c], x_up10[c], column_rms)
    print(np.sum(x2))

data = [x2, x5, x10, x_up10]

df = pd.DataFrame.from_records(data, columns = rms_df.columns)
df['RMS_interval'] = ['0-2', '2-5', '5-10', 'm10']

df.set_index('RMS_interval', inplace=True)
df['Pelvis Tilt'] = (df['Pelvis tilt']/df['Pelvis tilt'].sum())*100
df['Pelvis Obliquity'] = (df['Pelvis obliquity']/df['Pelvis obliquity'].sum())*100
df['Pelvis Rotation'] = (df['Pelvis rotation']/df['Pelvis rotation'].sum())*100
df['Hip Flex/Extension'] = (df['Hip flexion/extention']/df['Hip flexion/extention'].sum())*100
df['Hip Add/Abduction'] = (df['Hip adduction/abduction']/df['Hip adduction/abduction'].sum())*100
df['Hip Rotation'] = (df['Hip rotation']/df['Hip rotation'].sum())*100
df['Knee Flex/Extension'] = (df['Knee flexion/extention']/df['Knee flexion/extention'].sum())*100
df['Knee Valgus/Varus'] = (df['Knee valgus/varus']/df['Knee valgus/varus'].sum())*100
df['Knee Rotation'] = (df['Knee rotation']/df['Knee rotation'].sum())*100
df['Ankle Dorsi/Plantar Flexion'] = (df['Ankle dorsi/plantar flexion']/df['Ankle dorsi/plantar flexion'].sum())*100
df['Ankle Rotation'] = (df['Ankle rotation']/df['Ankle rotation'].sum())*100
df['Foot Progression'] = (df['Foot progression']/df['Foot progression'].sum())*100

print(df.columns)
# %%
rms_data_tdc = df.iloc[:,12:24]
dft_tdc = rms_data_tdc.transpose()


# %%
# data path
df = []
datapath_cp = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Results\\RMS\\CP\\"
RMS_list = glob.glob(datapath+'*.csv')
print(RMS_list)
z = [0]*12
x2 = [0]*12
x5 = [0]*12
x10 = [0]*12
x_up10 = [0]*12
print(x2)

def count_in_range(Z, A, B, C, D, column):
    for value in column:
        value = value * 1
        if value == 0:
            Z = Z + 1 
        elif value > 0 and value <2:
            A = A + 1
        elif value > 2 and value <5:
            B = B + 1
        elif  value > 5 and value <10:
            C = C + 1
        elif value >10:
            D = D + 1

    return Z, A, B, C, D

# loop read csv file
for file in RMS_list:
    rms_df = pd.read_csv(file)
    del rms_df['Simu']
    for c in range(0,12):
        col = rms_df.columns[c]
        # print(col)
        column_rms = (rms_df[col])
        [z[c], x2[c], x5[c], x10[c], x_up10[c]] = count_in_range(z[c], x2[c], x5[c], x10[c], x_up10[c], column_rms)
    print(np.sum(x2))

data = [x2, x5, x10, x_up10]

df = pd.DataFrame.from_records(data, columns = rms_df.columns)
df['RMS_interval'] = ['0-2', '2-5', '5-10', 'm10']

df.set_index('RMS_interval', inplace=True)
df['Pelvis Tilt'] = (df['Pelvis tilt']/df['Pelvis tilt'].sum())*100
df['Pelvis Obliquity'] = (df['Pelvis obliquity']/df['Pelvis obliquity'].sum())*100
df['Pelvis Rotation'] = (df['Pelvis rotation']/df['Pelvis rotation'].sum())*100
df['Hip Flex/Extension'] = (df['Hip flexion/extention']/df['Hip flexion/extention'].sum())*100
df['Hip Add/Abduction'] = (df['Hip adduction/abduction']/df['Hip adduction/abduction'].sum())*100
df['Hip Rotation'] = (df['Hip rotation']/df['Hip rotation'].sum())*100
df['Knee Flex/Extension'] = (df['Knee flexion/extention']/df['Knee flexion/extention'].sum())*100
df['Knee Valgus/Varus'] = (df['Knee valgus/varus']/df['Knee valgus/varus'].sum())*100
df['Knee Rotation'] = (df['Knee rotation']/df['Knee rotation'].sum())*100
df['Ankle Dorsi/Plantar Flexion'] = (df['Ankle dorsi/plantar flexion']/df['Ankle dorsi/plantar flexion'].sum())*100
df['Ankle Rotation'] = (df['Ankle rotation']/df['Ankle rotation'].sum())*100
df['Foot Progression'] = (df['Foot progression']/df['Foot progression'].sum())*100

print(df.columns)
# %%
rms_data_cp = df.iloc[:,12:24]
dft_cp = rms_data_cp.transpose()





# %% 
plt.figure(figsize=(15,15))
ax = dft.plot.bar(stacked = True, color=["green", "yellow", "orange","red"])
ax.legend(['0-2°', '2-5°', '5-10°', '>10°'],bbox_to_anchor = (1.27, 1), loc = 'upper right')
ax.set_ylim(0,100)
ax.set_ylabel('Percentage (%)')
plt.xticks(rotation=45)
# %%
pd.concat({'CP': dft_cp.set_index('Angle'), 'TDC': dft_tdc.set_index('Angle')}).plot.bar(stacked = True, color=["green", "yellow", "orange","red"])
# %%
df4 = pd.concat([dft_cp.T, dft_tdc.T], axis=0, ignore_index=False)
df4['col'] = (len(dft_cp.T)*(0,) + len(dft_tdc.T)*(1,))
df4.reset_index(inplace=True)
# %%
sns.factorplot(x = 'RMS_interval', y=['Pelvis Tilt', 'Pelvis Obliquity', 'Pelvis Rotation', 'Hip Flex/Extension', 'Hip Add/Abduction', 'Hip Rotation', 'Knee Flex/Extension', 'Knee Valgus/Varus', 'Knee Rotation', 'Ankle Dorsi/Plantar Flexion', 'Ankle Rotation', 'Foot Progression'], hue='col', kind='bar', data=df4)

# %%
fig = plt.figure(figsize=(15,15))
x1 = [i-0.1 for i in range(len(dft_cp))]
x2 = [i+0.1 for i in range(len(dft_tdc))]
for i,col in enumerate(dft_cp.columns):
    plt.bar(x=x1,height=dft_cp[col],width=0.2,label="CP")
    plt.bar(x=x2,height=dft_tdc[col],width=0.2,label="TDC")
    plt.legend()
# %%
