"""
author: M. Fonseca

Description: Routine to import GaitScores.csv and create a boxplot with the distribution of gait scores (GPS or GDI) per subject

"""


# %%
import pandas as pd
import numpy as np
import glob
import seaborn as sns
import matplotlib.pyplot as plt

# %%
datapath = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Results\\Gait_Scores\\TDC\\"

GaitScores_list = glob.glob(datapath+'*.csv')

# %%
v = np.arange(390026)
gs = pd.DataFrame(v)
gd = pd.DataFrame(v)
print(gs)
for file in GaitScores_list:
    df = pd.read_csv(file)
    pat_number = ['Subject_' + str(file[-6:-4])] 
    gs[pat_number] = df['GPS']
    gd[pat_number] = df['GDI']

    # if GS_df is not None:
    #     final_df = pd.concat([final_df, ])
# %%
gs2 = gs.iloc[:,1:]
gd2 = gd.iloc[:,1:]

# %%
bx = gs2.boxplot()
# bx.set_xticklabels(['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20'])
bx.set_xticklabels(['11','12','13','14','15','16','17','18','19','20'])

bx.set_title('GPS')
# %%
fig, axes = plt.subplots()
sns.violinplot(data=gs2, ax = axes)
axes.set_title('GPS distribution by misplacement')
axes.set_xlabel('Subject')
axes.set_ylabel('GPS')
axes.yaxis.grid()
# axes.set_xticklabels(['01','02','03','04','05','06','07','08','09','10'])
axes.set_xticklabels(['11','12','13','14','15','16','17','18','19','20'])
# axes.set_xticklabels(['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20'])


# %%
# %%
fig, axes = plt.subplots()
sns.violinplot(data=gd2, ax = axes)
axes.set_title('GDI distribution by misplacement')
axes.set_xlabel('Subject')
axes.set_ylabel('GDI')
axes.yaxis.grid()
# axes.set_xticklabels(['01','02','03','04','05','06','07','08','09','10'])
# axes.set_xticklabels(['11','12','13','14','15','16','17','18','19','20'])
axes.set_xticklabels(['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20'])

# %%
