# %%
"""
author: M. Fonseca

Description: Routine to calculate the inter-session variability for one subject and export it as .csv format.

 """

import pandas as pd
import File_Processing
import numpy as np
import glob

#%% 
datapath = "F:\\MarkerMisplacement_Data\\p09_c3d\\"

c3d_list = glob.glob(datapath+'\\'+'*'+'GBNNN'+'*.c3d')

# %%

Angle_names=['Pelvis', 'Hip', 'Knee', 'Ankle', 'FootProgress']
PT = []
PO = []
PR = []
HF = []
HA = []
HR = []
KF = []
KV = []
KR = []
AF = []
AR = []
FP = []

for i in range(len(c3d_list)):
    acq = File_Processing.reader(c3d_list[i])
    metadata = acq.GetMetaData()
    ff = acq.GetFirstFrame()
    Ev = acq.GetEvents()

    lfs = []
    for ev in range(Ev.GetItemNumber()):
        event = Ev.GetItem(ev)
        if event.GetLabel() == 'Foot Strike' and event.GetContext() == 'Left':
            print(event.GetLabel())
            lfs.append(event.GetFrame()-ff+1)
    for frame in range(len(lfs)-1):
        tn = np.linspace(0,100,101)
        xt = np.linspace(0, 100, len(acq.GetPoint('LPelvisAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],1]))
        PT = np.append(PT, np.interp(tn, xt, acq.GetPoint('LPelvisAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],0]))
        PO = np.append(PO, np.interp(tn, xt, acq.GetPoint('LPelvisAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],1]))
        PR = np.append(PR, np.interp(tn, xt, acq.GetPoint('LPelvisAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],2]))
        HF = np.append(HF, np.interp(tn, xt, acq.GetPoint('LHipAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],0]))
        HA = np.append(HA, np.interp(tn, xt, acq.GetPoint('LHipAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],1]))
        HR = np.append(HR, np.interp(tn, xt, acq.GetPoint('LHipAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],2]))
        KF = np.append(KF, np.interp(tn, xt, acq.GetPoint('LKneeAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],0]))
        KV = np.append(KV, np.interp(tn, xt, acq.GetPoint('LKneeAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],1]))
        KR = np.append(KR, np.interp(tn, xt, acq.GetPoint('LKneeAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],2]))
        AF = np.append(AF, np.interp(tn, xt, acq.GetPoint('LAnkleAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],0]))
        AR = np.append(AR, np.interp(tn, xt, acq.GetPoint('LAnkleAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],2]))
        FP = np.append(FP, np.interp(tn, xt, acq.GetPoint('LFootProgressAngles_pyCGM2_1').GetValues()[lfs[frame]:lfs[frame+1],2]))


# %%
PT = pd.DataFrame(np.reshape(PT, (int(len(PT)/101), 101)))
PO = pd.DataFrame(np.reshape(PO, (int(len(PO)/101), 101)))
PR = pd.DataFrame(np.reshape(PR, (int(len(PR)/101), 101)))

HF = pd.DataFrame(np.reshape(HF, (int(len(HF)/101),101)))
HA = pd.DataFrame(np.reshape(HA, (int(len(HA)/101),101)))
HR = pd.DataFrame(np.reshape(HR, (int(len(HR)/101),101)))

KF = pd.DataFrame(np.reshape(KF, (int(len(KF)/101),101)))
KV = pd.DataFrame(np.reshape(KV, (int(len(KV)/101),101)))
KR = pd.DataFrame(np.reshape(KR, (int(len(KR)/101),101)))

AF = pd.DataFrame(np.reshape(AF, (int(len(AF)/101),101)))
AR = pd.DataFrame(np.reshape(AR, (int(len(AR)/101),101)))
FP = pd.DataFrame(np.reshape(FP, (int(len(FP)/101),101)))
# %%
std = pd.DataFrame()
std['Pelvis tilt'] = PT.std(axis=0)
std['Pelvis obliquity'] = PO.std(axis=0)
std['Pelvis rotation'] = PR.std(axis=0)
std['Hip flexion/extention'] = HF.std(axis=0)
std['Hip adduction/abduction'] = HA.std(axis=0)
std['Hip rotation'] = HR.std(axis=0)
std['Knee flexion/extention'] = KF.std(axis=0)
std['Knee valgus/varus'] = KV.std(axis=0)
std['Knee rotation'] = KR.std(axis=0)
std['Ankle dorsi/plantar flexion'] = AF.std(axis=0)
std['Ankle rotation'] = AR.std(axis=0)
std['Foot progression'] = FP.std(axis=0)
# %%
std.to_csv('Intra_session_variability.csv')
# %%
