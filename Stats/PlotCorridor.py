"""
author: M. Fonseca

Description: Import max, min and intra-session variability and plot those values over the original mean angle.

"""
#%% 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import (AutoMinorLocator, MultipleLocator)
from pandas.core.resample import resample

#%%
df_ref = pd.read_csv("F:\\MarkerMisplacement_Data\\Patient09\\p09_pyCGM1.csv", sep=';')
df_ref = df_ref.truncate(before = 4, after= 107)
df_ref = df_ref[['Pelvis tilt','Pelvis obliquity','Pelvis rotation', 'Hip flexion/extention','Hip adduction/abduction','Hip rotation','Knee flexion/extention','Knee valgus/varus','Knee rotation','Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']]
df_ref = df_ref.reset_index(drop=True)

df_max = pd.read_csv("C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Stats\\p09_max.csv")
df_min = pd.read_csv("C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Stats\\p09_min.csv")
df_std = pd.read_csv("C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Stats\\Intra_session_variability.csv")

list_of_angles = ['Pelvis tilt','Pelvis obliquity','Pelvis rotation', 'Hip flexion/extention','Hip adduction/abduction','Hip rotation','Knee flexion/extention','Knee valgus/varus','Knee rotation','Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']
# %%
PT = df_ref['Pelvis tilt']


# %%


x = np.linspace(0, 100, 100)
xp = np.linspace(0,100,len(df_ref['Pelvis tilt']))

fig, axs = plt.subplots(5,3, figsize=(20,15))
plt.rc('font', size = 12)

axs1 = plt.subplot(5,3,1)
axs1.plot(x, np.interp(x,xp,df_ref['Pelvis tilt']), 'black')
axs1.fill_between(x, np.interp(x,xp,df_max['Pelvis tilt']), np.interp(x,xp,df_min['Pelvis tilt']), facecolor = 'orange', alpha = 0.5)
axs1.fill_between(x, (df_std['Pelvis tilt'][:100]+np.interp(x,xp,df_ref['Pelvis tilt'])),(np.interp(x,xp,df_ref['Pelvis tilt'])-df_std['Pelvis tilt'][:100]), facecolor = 'green', alpha = 0.5)
axs1.set_ylim([-20, 20])
axs1.set_xlim([0, 100])
axs1.title.set_text('Pelvis Tilt')

axs2 = plt.subplot(5,3,2)
axs2.plot(x, np.interp(x,xp,df_ref['Pelvis obliquity']), 'black')
axs2.fill_between(x, np.interp(x,xp,df_max['Pelvis obliquity']), np.interp(x,xp,df_min['Pelvis obliquity']), facecolor = 'orange', alpha = 0.5)
axs2.fill_between(x, (df_std['Pelvis obliquity'][:100]+np.interp(x,xp,df_ref['Pelvis obliquity'])),(np.interp(x,xp,df_ref['Pelvis obliquity'])-df_std['Pelvis obliquity'][:100]), facecolor = 'green', alpha = 0.5)
axs2.set_ylim([-20, 20])
axs2.set_xlim([0, 100])
axs2.title.set_text('Pelvis Obliquity')

axs3 = plt.subplot(5,3,3)
axs3.plot(x, np.interp(x,xp,df_ref['Pelvis rotation']), 'black', label='Original')
axs3.fill_between(x, (df_std['Pelvis rotation'][:100]+np.interp(x,xp,df_ref['Pelvis rotation'])),(np.interp(x,xp,df_ref['Pelvis rotation'])-df_std['Pelvis rotation'][:100]), facecolor = 'green', alpha = 0.5, label = 'Inter-trial variability')
axs3.fill_between(x, np.interp(x,xp,df_max['Pelvis rotation']), np.interp(x,xp,df_min['Pelvis rotation']), facecolor = 'orange', alpha = 0.91, label = 'Error corridor')
axs3.set_ylim([-20, 20])
axs3.set_xlim([0, 100])
axs3.title.set_text('Pelvis Rotation')
axs3.legend()

axs4 = plt.subplot(5,3,4)
axs4.plot(x, np.interp(x,xp,df_ref['Hip flexion/extention']), 'black')
axs4.fill_between(x, np.interp(x,xp,df_max['Hip flexion/extention']), np.interp(x,xp,df_min['Hip flexion/extention']), facecolor = 'orange', alpha = 0.5)
axs4.fill_between(x, (df_std['Hip flexion/extention'][:100]+np.interp(x,xp,df_ref['Hip flexion/extention'])),(np.interp(x,xp,df_ref['Hip flexion/extention'])-df_std['Hip flexion/extention'][:100]), facecolor = 'green', alpha = 0.5)
axs4.set_ylim([-20, 60])
axs4.set_xlim([0, 100])
axs4.title.set_text('Hip Flex/Extension')

axs5 = plt.subplot(5,3,5)
axs5.plot(x, np.interp(x,xp,df_ref['Hip adduction/abduction']), 'black')
axs5.fill_between(x, np.interp(x,xp,df_max['Hip adduction/abduction']), np.interp(x,xp,df_min['Hip adduction/abduction']), facecolor = 'orange', alpha = 0.5)
axs5.fill_between(x, (df_std['Hip adduction/abduction'][:100]+np.interp(x,xp,df_ref['Hip adduction/abduction'])),(np.interp(x,xp,df_ref['Hip adduction/abduction'])-df_std['Hip adduction/abduction'][:100]), facecolor = 'green', alpha = 0.5)
axs5.set_ylim([-20, 20])
axs5.set_xlim([0, 100])
axs5.title.set_text('Hip Add/Abduction')

axs6 = plt.subplot(5,3,6)
axs6.plot(x, np.interp(x,xp,df_ref['Hip rotation']), 'black')
axs6.fill_between(x, np.interp(x,xp,df_max['Hip rotation']), np.interp(x,xp,df_min['Hip rotation']), facecolor = 'orange', alpha = 0.5)
axs6.fill_between(x, (df_std['Hip rotation'][:100]+np.interp(x,xp,df_ref['Hip rotation'])),(np.interp(x,xp,df_ref['Hip rotation'])-df_std['Hip rotation'][:100]), facecolor = 'green', alpha = 0.5)
axs6.set_ylim([-30, 30])
axs6.set_xlim([0, 100])
axs6.title.set_text('Hip Rotation')

axs7 = plt.subplot(5,3,7)
axs7.plot(x, np.interp(x,xp,df_ref['Knee flexion/extention']), 'black')
axs7.fill_between(x, np.interp(x,xp,df_max['Knee flexion/extention']), np.interp(x,xp,df_min['Knee flexion/extention']), facecolor = 'orange', alpha = 0.5)
axs7.fill_between(x, (df_std['Knee flexion/extention'][:100]+np.interp(x,xp,df_ref['Knee flexion/extention'])),(np.interp(x,xp,df_ref['Knee flexion/extention'])-df_std['Knee flexion/extention'][:100]), facecolor = 'green', alpha = 0.5)
axs7.set_ylim([-10, 80])
axs7.set_xlim([0, 100])
axs7.title.set_text('Knee Flex/Extension')

axs8 = plt.subplot(5,3,8)
axs8.plot(x, np.interp(x,xp,df_ref['Knee valgus/varus']), 'black')
axs8.fill_between(x, np.interp(x,xp,df_max['Knee valgus/varus']), np.interp(x,xp,df_min['Knee valgus/varus']), facecolor = 'orange', alpha = 0.5)
axs8.fill_between(x, (df_std['Knee valgus/varus'][:100]+np.interp(x,xp,df_ref['Knee valgus/varus'])),(np.interp(x,xp,df_ref['Knee valgus/varus'])-df_std['Knee valgus/varus'][:100]), facecolor = 'green', alpha = 0.5)
axs8.set_ylim([-20, 20])
axs8.set_xlim([0, 100])
axs8.title.set_text('Knee Var/Valgus')

axs9 = plt.subplot(5,3,9)
axs9.plot(x, np.interp(x,xp,df_ref['Knee rotation']), 'black')
axs9.fill_between(x, np.interp(x,xp,df_max['Knee rotation']), np.interp(x,xp,df_min['Knee rotation']), facecolor = 'orange', alpha = 0.5)
axs9.fill_between(x, (df_std['Knee rotation'][:100]+np.interp(x,xp,df_ref['Knee rotation'])),(np.interp(x,xp,df_ref['Knee rotation'])-df_std['Knee rotation'][:100]), facecolor = 'green', alpha = 0.5)
axs9.set_ylim([-30, 30])
axs9.set_xlim([0, 100])
axs9.title.set_text('Knee Rotation')

axs10 = plt.subplot(5,3,10)
axs10.plot(x, np.interp(x,xp,df_ref['Ankle dorsi/plantar flexion']), 'black')
axs10.fill_between(x, np.interp(x,xp,df_max['Ankle dorsi/plantar flexion']), np.interp(x,xp,df_min['Ankle dorsi/plantar flexion']), facecolor = 'orange', alpha = 0.5)
axs10.fill_between(x, (df_std['Ankle dorsi/plantar flexion'][:100]+np.interp(x,xp,df_ref['Ankle dorsi/plantar flexion'])),(np.interp(x,xp,df_ref['Ankle dorsi/plantar flexion'])-df_std['Ankle dorsi/plantar flexion'][:100]), facecolor = 'green', alpha = 0.5)
axs10.set_ylim([-30, 30])
axs10.set_xlim([0, 100])
axs10.title.set_text('Ankle Flex/Extension')

axs11 = plt.subplot(5,3,12)
axs11.plot(x, np.interp(x,xp,df_ref['Ankle rotation']), 'black')
axs11.fill_between(x, np.interp(x,xp,df_max['Ankle rotation']), np.interp(x,xp,df_min['Ankle rotation']), facecolor = 'orange', alpha = 0.5)
axs11.fill_between(x, (df_std['Ankle rotation'][:100]+np.interp(x,xp,df_ref['Ankle rotation'])),(np.interp(x,xp,df_ref['Ankle rotation'])-df_std['Ankle rotation'][:100]), facecolor = 'green', alpha = 0.5)
axs11.set_ylim([-30, 30])
axs11.set_xlim([0, 100])
axs11.title.set_text('Ankle Rotation')

axs12 = plt.subplot(5,3,15)
axs12.plot(x, np.interp(x,xp,df_ref['Foot progression']), 'black')
axs12.fill_between(x, np.interp(x,xp,df_max['Foot progression'])-1.5, np.interp(x,xp,df_min['Foot progression'])-1.5, facecolor = 'orange', alpha = 0.5)
axs12.fill_between(x, (df_std['Foot progression'][:100]+np.interp(x,xp,df_ref['Foot progression'])),(np.interp(x,xp,df_ref['Foot progression'])-df_std['Foot progression'][:100]), facecolor = 'green', alpha = 0.5)
axs12.set_ylim([-30, 10])
axs12.set_xlim([0, 100])
axs12.title.set_text('Foot Progression')
# %%
