

"""
author: M. Fonseca

Description: Go through all RMS (csv) of one patient and export min and max values per point of gait cycle and angle

"""
# %%
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
datapath = "F:\\MarkerMisplacement_Data\\Patient09\\"
# name reference csv
ref = '_pyCGM1.csv'
df_max = pd.read_csv(datapath+'\\p09_simu'+str(1) +'.csv', sep=';')
df_max = df_max.truncate(before = 4, after= 107)
df_min = df_max.copy()
# df_max = pd.read_csv("p09_max.csv")
df_max.drop(df_max.columns[[0]], axis = 1, inplace=True)
# df_max.insert(loc=0, column = 'index', value=np.arange(104))
# df_max.set_index('index')
# df_max = df_max.iloc[:,0:12]
df_max = df_max[['Pelvis tilt','Pelvis obliquity','Pelvis rotation', 'Hip flexion/extention','Hip adduction/abduction','Hip rotation','Knee flexion/extention','Knee valgus/varus','Knee rotation','Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']]
df_max = df_max.reset_index(drop=True)
# df_min = pd.read_csv("p09_min.csv")
df_min.drop(df_min.columns[[0]], axis = 1, inplace=True)
# df_min = df_min.iloc[:,0:12]
df_min = df_min[['Pelvis tilt','Pelvis obliquity','Pelvis rotation', 'Hip flexion/extention','Hip adduction/abduction','Hip rotation','Knee flexion/extention','Knee valgus/varus','Knee rotation','Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']]
df_min = df_min.reset_index(drop= True)
for sim in range(2,390027):
    print('Simulation' + str(sim))
    df_sim = pd.read_csv(datapath+'\\p09_simu'+str(sim) +'.csv', sep=';') 
    
    if df_sim.shape[0] > 115:
        df_sim = df_sim.truncate(before = 8, after=111)
        df_sim = df_sim.iloc[:,1:13]
        df_sim = df_sim.reset_index(drop=True)
    else:
        df_sim = df_sim.truncate(before = 4, after=107)
   
    # df_max = pd.concat([df_max, df_sim]).max(level=0)
    # df_max = pd.concat([df_max, df_sim]).min(level=0)
    dfm = df_max.iloc[:,0:12]
    dfs = df_sim.iloc[:,0:12]
    dfs = df_sim[['Pelvis tilt','Pelvis obliquity','Pelvis rotation', 'Hip flexion/extention','Hip adduction/abduction','Hip rotation','Knee flexion/extention','Knee valgus/varus','Knee rotation','Ankle dorsi/plantar flexion','Ankle rotation','Foot progression']]
    dfs = dfs.reset_index(drop=True)

    dfmi = df_min.iloc[:,0:12]
    df_max = dfm.where(dfm > dfs, dfs)
    df_min = dfmi.where(dfmi < dfs, dfs)
    
    if sim % 1000 == 0 or sim > 390000:
        df_max.to_csv("p09_max.csv", index=False)
        df_min.to_csv("p09_min.csv", index=False)
