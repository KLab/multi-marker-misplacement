"""
author: M. Fonseca

Description: Routine to create a heatmap plot to illustrate the impact of single marker misplacement on all angle kinematics

"""

import pandas as pd
import numpy as np
import seaborn as sns
import glob 
import matplotlib.pyplot as plt

# %%
# call the list of csv files
datapath = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Results\\RMS\\CP\\"
RMS_list = glob.glob(datapath+'*.csv')

# define the number of simulations to be extracted
LASI_sim = ['simu78125', 'simu156250', 'simu234375', 'simu312500']
RASI_sim = ['simu15625', 'simu31250', 'simu46875', 'simu62500']
SACR_sim = ['simu3125', 'simu6250', 'simu9375','simu12500']
LTHI_sim = ['simu625', 'simu1250', 'simu1875', 'simu2500']
LKNE_sim = ['simu125', 'simu250', 'simu375', 'simu500']
LTIB_sim = ['simu25', 'simu50', 'simu75', 'simu100']
LANK_sim = ['simu5', 'simu10', 'simu15', 'simu20']
LTOE_sim = ['simu1', 'simu2', 'simu3', 'simu4']
misp = [0, 90, 180, 270]
all_sim = LASI_sim + RASI_sim + SACR_sim + LTHI_sim + LKNE_sim + LTIB_sim + LANK_sim + LTOE_sim
mark_misp = ['LTOE_0', 'LTOE_90', 'LTOE_180', 'LTOE_270', 'LANK_0', 'LANK_90', 'LANK_180', 'LANK_270', 'LTIB_0', 'LTIB_90', 'LTIB_180', 'LTIB_270','LKNE_0', 'LKNE_90', 'LKNE_180', 'LKNE_270', 'LTHI_0', 'LTHI_90', 'LTHI_180', 'LTHI_270', 'SACR_0', 'SACR_90', 'SACR_180', 'SACR_270','RASI_0', 'RASI_90', 'RASI_180', 'RASI_270', 'LASI_0', 'LASI_90', 'LASI_180', 'LASI_270']
# %% 
# Initialize dataframe
df = pd.DataFrame(columns=['Subject', 'Marker', 'Direction Misplacement','Simu', 'Pelvis tilt', 'Pelvis obliquity', 'Pelvis rotation', 'Hip flexion/extention', 'Hip adduction/abduction', 'Hip rotation', 'Knee flexion/extention', 'Knee valgus/varus', 'Knee rotation', 'Ankle dorsi/plantar flexion', 'Ankle rotation', 'Foot progression'])
final_df = None
for file in RMS_list:
   rms_df = pd.read_csv(file) 
   pat_number = file[-6:-4] 
   count = 0
   rslt_df = rms_df.loc[rms_df['Simu'].isin(all_sim)]
   rslt_df.columns = ['simu', 'Pelvis tilt - '+str(pat_number),'Pelvis obliquity - '+str(pat_number), 'Pelvis rotation - '+str(pat_number), 'Hip flex/extention - '+str(pat_number),'Hip add/abduction - '+str(pat_number),'Hip rotation - '+str(pat_number), 'Knee flex/extention - '+str(pat_number),'Knee val/varus - '+str(pat_number),'Knee rotation - '+str(pat_number),'Ankle flex/extension - '+str(pat_number),'Ankle add/abduction - '+str(pat_number),'Foot progress - '+str(pat_number)]
   rslt_df['Marker Misplacement'] = mark_misp
#    rslt_df['Subject'] = np.repeat(pat_number, 32)

   if final_df is not None:
       final_df = pd.concat([final_df, rslt_df], axis=1)
   else:
       final_df = rslt_df.copy()
# final_df.set_index('Marker Misplacement', inplace=True)

# %%
PT = final_df.filter(regex='Pelvis tilt').mean(axis=1)
PO = final_df.filter(regex='Pelvis obliquity').mean(axis=1)
PR = final_df.filter(regex='Pelvis rotation').mean(axis=1)
HF = final_df.filter(regex='Hip flex').mean(axis=1)
HA = final_df.filter(regex='Hip add').mean(axis=1)
HR = final_df.filter(regex='Hip rotation').mean(axis=1)
HR_std = final_df.filter(regex='Hip rotation').std(axis=1)
KF = final_df.filter(regex='Knee flex').mean(axis=1)
KV = final_df.filter(regex='Knee va').mean(axis=1)
KR = final_df.filter(regex='Knee rotation').mean(axis=1)
AF = final_df.filter(regex='Ankle flex').mean(axis=1)
AA = final_df.filter(regex='Ankle add').mean(axis=1)
FP = final_df.filter(regex='Foot').mean(axis=1)

HM_df = pd.concat([rslt_df['Marker Misplacement'], PT, PO, PR, HF, HA, HR, KF, KV, KR, AF, AA, FP], axis=1)
HM_df.set_index('Marker Misplacement', inplace=True)
HM_df.columns = ['Pelvis Tilt', 'Pelvis Obliquity', 'Pelvis Rotation', 'Hip Flex/Extension', 'Hip Add/Abduction', 'Hip Rotation', 'Knee Flex/Extension', 'Knee Valgus/Varus', 'Knee Rotation', 'Ankle Dorsi/Plantar Flexion', 'Ankle Rotation', 'Foot Progression']
# %%
from matplotlib.pyplot import figure
plt.figure(figsize=(10,10))
ax = sns.heatmap(HM_df, cmap='RdYlGn_r', annot=True)
plt.xticks(rotation=45)
# %%
