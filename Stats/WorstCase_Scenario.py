
"""
author: M. Fonseca

Description: Get the "num_worst" worst case scenario and get its respective simulation number. Export as csv individually (for each angle)

"""
# %%
import glob
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

# %%
datapath = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Results\\RMS\\CP\\"
RMS_list = glob.glob(datapath+'*.csv')
RMS_df = None
for file in RMS_list:
    rms_df = pd.read_csv(file)
    if RMS_df is not None:
        RMS_df = pd.concat([RMS_df, rms_df])
    else:
        RMS_df = rms_df.copy()

# %%
idx = pd.DataFrame()
num_worst = 10
max_PT = RMS_df.sort_values(by=['Pelvis tilt'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['PT'] = max_PT['Simu'].values

max_PO = RMS_df.sort_values(by=['Pelvis obliquity'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['PO'] = max_PO['Simu'].values

max_PR = RMS_df.sort_values(by=['Pelvis rotation'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['PR'] = max_PR['Simu'].values

max_HF = RMS_df.sort_values(by=['Hip flexion/extention'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['HF'] = max_HF['Simu'].values

max_HA = RMS_df.sort_values(by=['Hip adduction/abduction'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['HA'] = max_HA['Simu'].values

max_HR = RMS_df.sort_values(by=['Hip rotation'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['HR'] = max_HR['Simu'].values

max_KF = RMS_df.sort_values(by=['Knee flexion/extention'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['KF'] = max_KF['Simu'].values

max_KV = RMS_df.sort_values(by=['Knee valgus/varus'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['KV'] = max_KV['Simu'].values

max_KR = RMS_df.sort_values(by=['Knee rotation'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['KR'] = max_KR['Simu'].values

max_AF = RMS_df.sort_values(by=['Ankle dorsi/plantar flexion'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['AF'] = max_AF['Simu'].values

max_AR = RMS_df.sort_values(by=['Ankle rotation'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['AR'] = max_AR['Simu'].values

max_FP = RMS_df.sort_values(by=['Foot progression'], ascending=False).iloc[:num_worst,:].replace('simu','', regex=True)
idx['FP'] = max_FP['Simu'].values
# %%
# Load combinations
combi_path = "C:\\Users\\FONSECMI\\OneDrive - unige.ch\\Projects\\MIS\\MIS2\\Code\\Results\\"
combi_df = pd.read_csv(combi_path + 'combinaisons.txt')
combi_df = combi_df.drop(columns=' arrow')
# %%
combi_df =combi_df['Simu'].str.contains("[")
# %%
combi_df['Simu'] = combi_df['Simu'].str.replace(r'[', '')
combi_df['Simu'] = combi_df['Simu'].str.replace(r'simu', '', regex=True)
cols = combi_df.columns
for i in cols:
    combi_df[i] = combi_df[i].str.replace(r'[', '')
    combi_df[i] = combi_df[i].str.replace(r']', '')
# %%
PT_misp = combi_df[combi_df['Simu'].isin(idx['PT'])]
PO_misp = combi_df[combi_df['Simu'].isin(idx['PO'])]
PR_misp = combi_df[combi_df['Simu'].isin(idx['PR'])]
HF_misp = combi_df[combi_df['Simu'].isin(idx['HF'])]
HA_misp = combi_df[combi_df['Simu'].isin(idx['HA'])]
HR_misp = combi_df[combi_df['Simu'].isin(idx['HR'])]
KF_misp = combi_df[combi_df['Simu'].isin(idx['KF'])]
KV_misp = combi_df[combi_df['Simu'].isin(idx['KV'])]
KR_misp = combi_df[combi_df['Simu'].isin(idx['KR'])]
AF_misp = combi_df[combi_df['Simu'].isin(idx['AF'])]
AR_misp = combi_df[combi_df['Simu'].isin(idx['AR'])]
FP_misp = combi_df[combi_df['Simu'].isin(idx['FP'])]
# %%

PT_misp['RMS'] = max_PT['Pelvis tilt']
PO_misp['RMS'] = max_PO['Pelvis obliquity']
PR_misp['RMS'] = max_PR['Pelvis rotation']

HF_misp['RMS'] = max_HF['Hip flexion/extention']
HA_misp['RMS'] = max_HA['Hip adduction/abduction']
HR_misp['RMS'] = max_HR['Hip rotation']

KF_misp['RMS'] = max_KF['Knee flexion/extention']
KV_misp['RMS'] = max_KV['Knee valgus/varus']
KR_misp['RMS'] = max_KR['Knee rotation']

AF_misp['RMS'] = max_AF['Ankle dorsi/plantar flexion']
AR_misp['RMS'] = max_AR['Ankle rotation']
FP_misp['RMS'] = max_FP['Foot progression']

# %% 
PT_misp.to_csv('PT_misp_CP.csv')
PO_misp.to_csv('PO_misp_CP.csv')
PR_misp.to_csv('PR_misp_CP.csv')

HF_misp.to_csv('HF_misp_CP.csv')
HA_misp.to_csv('HA_misp_CP.csv')
HR_misp.to_csv('HR_misp_CP.csv')

KF_misp.to_csv('KF_misp_CP.csv')
KV_misp.to_csv('KV_misp_CP.csv')
KR_misp.to_csv('KR_misp_CP.csv')

AF_misp.to_csv('AF_misp_CP.csv')
AR_misp.to_csv('AR_misp_CP.csv')
FP_misp.to_csv('FP_misp_CP.csv')
# %%
